#!/usr/bin/python

""" 
    Starter code for exploring the Enron dataset (emails + finances);
    loads up the dataset (pickled dict of dicts).

    The dataset has the form:
    enron_data["LASTNAME FIRSTNAME MIDDLEINITIAL"] = { features_dict }

    {features_dict} is a dictionary of features associated with that person.
    You should explore features_dict as part of the mini-project,
    but here's an example to get you started:

    enron_data["SKILLING JEFFREY K"]["bonus"] = 5600000
    
"""

import pickle

enron_data = pickle.load(open("../final_project/final_project_dataset.pkl", "r"))
total = len(enron_data)
print total

print len(enron_data['GLISAN JR BEN F'].keys())
poi = float(len([k for k, v in enron_data.iteritems() if v['poi']]))
print "poi:" + str(poi)
print "salary"
print len([k for k, v in enron_data.iteritems() if v['salary'] != 'NaN'])
print "addr"
print len([k for k, v in enron_data.iteritems() if v['email_address'] != 'NaN'])
print "total pay"
totalpay = float(len([k for k, v in enron_data.iteritems() if v['total_payments'] == 'NaN']))
print  totalpay / float(total)
nanpoi = len([k for k, v in enron_data.iteritems() if v['poi'] == 'NaN'])
print "poi/nontotalpay"

nantotalpay = len([k for k, v in enron_data.iteritems() if v['total_payments'] == 'NaN'])
print "nantotalpay:" + str(nantotalpay)
poinantotalpay = len([k for k, v in enron_data.iteritems() if v['total_payments'] == 'NaN' and v['poi']])
print "poinantotalpay:" + str(poinantotalpay)


with open('../final_project/poi_names.txt') as f:
  data = f.read().split('\n')
  print len(data) -3
#print str(sorted(enron_data.keys()))
#print str(enron_data['PRENTICE JAMES'])
#print str(enron_data['Colwell Wesley'.upper()])
#print str(enron_data['Skilling Jeffrey K'.upper()])
#8682716,
#print str(enron_data['LAY KENNETH L'.upper()])
#103559793,
#print str(enron_data['FASTOW ANDREW S'.upper()])
#2424083,
