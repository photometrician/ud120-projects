#!/usr/bin/python

import pickle
import sys
import matplotlib.pyplot
sys.path.append("../tools/")
from feature_format import featureFormat, targetFeatureSplit


### read in data dictionary, convert to numpy array
data_dict = pickle.load( open("../final_project/final_project_dataset.pkl", "r") )
#print data_dict
features = ["salary", "bonus"]
data = featureFormat(data_dict, features)


### your code below
def get_key(d):
  return d["bonus"]
#print data
#print sorted(data_dict, key=get_key)
for k, v in data_dict.iteritems():
  if v["bonus"] == "NaN" or v["salary"] == "NaN":
    continue
  if v["bonus"] > 5000000 and v["salary"] > 1000000:
    print k, str(v)
#[  3.39288000e+05   8.00000000e+06]

