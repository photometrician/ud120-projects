#!/usr/bin/python

#import math

def outlierCleaner(predictions, ages, net_worths):
    """
        Clean away the 10% of points that have the largest
        residual errors (difference between the prediction
        and the actual net worth).

        Return a list of tuples named cleaned_data where 
        each tuple is of the form (age, net_worth, error).
    """
    def get_key(item):
      return item[1]

    cleaned_data = []

    ### your code goes here
    elms = []
    for idx, val in enumerate(ages):
      elms.append((idx, abs(predictions[idx] - net_worths[idx])))
    sorted_elms = sorted(elms, key=get_key)
    cut_elms = sorted_elms[:int(len(sorted_elms) * 0.9)]
    print "cutnum: " + str(len(cut_elms))
    for idx, val in cut_elms:
      cleaned_data.append((ages[idx], net_worths[idx], val))
    return cleaned_data

